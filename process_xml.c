//
// Created by martin on 1/26/2021.
//

#include <libxml/HTMLparser.h>
#include <libxml/xpath.h>

#include <string.h>

#include "cbazos.h"

void extract_text(xmlNode *node, char **output) {
    while (node) {
      if (node->type == XML_TEXT_NODE) {
        char *content = (char *) xmlNodeGetContent(node);
        concat(output, content);
      } else {
        extract_text(node->children, output);
      }
      node = node->next;
    }
}

void print_xml(xmlNode *node, int indent_len) {

    while (node) {
        if (node->type == XML_TEXT_NODE) {
            printf("%*c%s:%s\n", indent_len * 2, '-', node->parent->name, xmlNodeGetContent(node));
            //is_leaf(node)?xmlNodeGetContent(node):xmlGetProp(node, "id"));
        }
        print_xml(node->children, indent_len + 1);
        node = node->next;
    }
}

int process_xml_memory_buffer(struct collected_data_item *item, struct entry_config *entries, int entries_count) {
    htmlDocPtr doc;
    xmlXPathContextPtr xpathCtx;
    xmlXPathObjectPtr xpathObj;


    xmlInitParser();
    doc = htmlReadMemory(item->xml_buffer.memory, item->xml_buffer.size, "", NULL,
                         HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING | HTML_PARSE_NONET);
    if (doc == NULL) {

        fprintf(stderr, "Error: unable to parse data\n");
        return -1;
    }


    for (int j = 0; j < entries_count; j++) {
        struct collected_data_entry *entry = &item->entry_array.items[j];
        entry->name = entries[j].name;
        entry->error = NULL;
        entry->value = NULL;
        entry->filtered_value = NULL;

        xpathCtx = xmlXPathNewContext(doc);
        if (xpathCtx == NULL) {
            entry->error = "Error: unable to create new XPath context";
            continue;
        }

        xpathObj = xmlXPathEvalExpression((unsigned char *) entries[j].query, xpathCtx);
        if (xpathObj == NULL) {
            entry->error = "Error: unable to evaluate xpath expression";
            xmlXPathFreeContext(xpathCtx);
            continue;
        }
        xmlNodeSetPtr nodeset = xpathObj->nodesetval;
        if (xmlXPathNodeSetIsEmpty(nodeset)) {
            entry->error = "nothing found for query";
            xmlXPathFreeObject(xpathObj);
            xmlXPathFreeContext(xpathCtx);
            continue;
        }

        if (strcmp(entry->name, "test") == 0) {
            for (int i = 0; i < nodeset->nodeNr; i++) {
                print_xml(nodeset->nodeTab[i]->children, 0);
            }
        }

        if (nodeset->nodeNr > 1) {
            entry->error = "More then one result for query, using first";

        }

        extract_text(nodeset->nodeTab[0]->children, &entry->value);
        entry->filtered_value = entry_value_filter(entry->value, &entries[j], &entry->error);

        xmlXPathFreeObject(xpathObj);
        xmlXPathFreeContext(xpathCtx);
    }

    xmlCleanupParser();
    xmlFreeDoc(doc);

    return 1;
}


