//
// Created by martin on 1/16/2021.
//

#ifndef CBAZOS_CONFIG_H
#define CBAZOS_CONFIG_H

#include "types.h"

#define RSS_URL "rss_url"
#define RSS_TIMEOUT "rss_timeout"
#define RECORD_ENTRIES "entries"
#define OUTPUT "output"
#define FETCH_STORE_DIR "fetch_store_dir"

int parse_config(struct fetch_config *rss_config, const char *file_name);

void clean_config(struct fetch_config *rss_config);

#endif //CBAZOS_CONFIG_H
