//
// Created by martin on 1/26/2021.
//

#ifndef CBAZOS_CBAZOS_H
#define CBAZOS_CBAZOS_H

#include "types.h"

int process_xml_memory_buffer(struct collected_data_item *item, struct entry_config *entries, int entries_count);

char *entry_value_filter(char *value, struct entry_config *config, char **error_msg);

void clean_data_collection_item(struct collected_data_item *item);

void concat(char **str, char *str2);

#endif //CBAZOS_CBAZOS_H
