//
// Created by martin on 1/28/2021.
//

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <regex.h>

#include "cbazos.h"


void clean_data_collection_entry(struct collected_data_entry *entry) {
    if (entry->error != NULL) {
        free(entry->error);
    }
    if (entry->filtered_value != NULL) {
        free(entry->filtered_value);
    }
    if (entry->value != NULL) {
        free(entry->value);
    }
}

void clean_data_collection_item(struct collected_data_item *item) {
    for (int j = 0; j < item->entry_array.size; j++) {
        clean_data_collection_entry(&item->entry_array.items[j]);
    }
    if (item->entry_array.size > 0) {
        free(item->entry_array.items);
    }
    item->entry_array.size = 0;
}


char *trim_whitespace(char *str) {
    char *end;

    // Trim leading space
    while (isspace((unsigned char) *str)) str++;

    if (*str == 0)  // All spaces?
        return str;

    // Trim trailing space
    end = str + strlen(str) - 1;
    while (end > str && isspace((unsigned char) *end)) end--;

    return strndup(str, end - str + 1);
}

void concat(char **str, char *str2) {

    if (str2 == NULL) {
        return;
    }

    if (*str == NULL) {
        *str = str2;
    } else {
        size_t str_len = strlen(*str);
        size_t str2_len = strlen(str2);
        if (str2_len == 0) {
            return;
        }

        *str = realloc(*str, str_len + str2_len + 1);
        strcat(*str, str2);
        free(str2);
    }
}

char *posix_regex(const char *str, const char *expr, char **error_msg) {
    regex_t regex;
    regmatch_t r_match;
    char *tmp_msg;
    int ret;
    ret = regcomp(&regex, expr, REG_EXTENDED);
    if (ret != 0) {
        size_t msg_size = regerror(ret, &regex, NULL, 0);
        tmp_msg = malloc(msg_size);
        if (regerror(ret, &regex, tmp_msg, msg_size) != msg_size) {
            fprintf(stderr, "could not create error message correctly\n");
        }
        concat(error_msg, tmp_msg);
        regfree(&regex);

        return NULL;
    }

    ret = regexec(&regex, str, 1, &r_match, 0);
    if (ret != 0) {
        size_t msg_size = regerror(ret, &regex, NULL, 0);
        tmp_msg = malloc(msg_size);
        if (regerror(ret, &regex, tmp_msg, msg_size) != msg_size) {
            fprintf(stderr, "could not create error message correctly\n");
        }
        concat(error_msg, tmp_msg);
        regfree(&regex);
        return NULL;
    }
    regfree(&regex);
    char *result = strndup(str + r_match.rm_so, r_match.rm_eo - r_match.rm_so);

    return result;
}

char *entry_value_filter(char *value, struct entry_config *config, char **error_msg) {
    switch (config->filter.type) {
        case trim:
            return trim_whitespace(value);
        case replace:
        case regex:
            return posix_regex(value, config->filter.options, error_msg);
        case query:
        default:
            break;
    }
    return NULL;
}

