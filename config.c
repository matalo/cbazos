//
// Created by martin on 1/16/2021.
//
#include <libconfig.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "config.h"

static struct {
    enum filter_type val;
    const char *str;
} conversion[] = {
        {query,   "query"},
        {regex,   "regex"},
        {replace, "replace"},
        {trim,    "trim"},
};

enum filter_type str2enum(const char *str) {
    long unsigned int j;
    for (j = 0; j < sizeof(conversion) / sizeof(conversion[0]); ++j)
        if (!strcmp(str, conversion[j].str))
            return conversion[j].val;
    return 0;
}

int parse_config(struct fetch_config *rss_config, const char *file_name) {
    config_t cfg;
    config_setting_t *setting;
    const char *str;


    config_init(&cfg);

    if (access(file_name, F_OK) != 0) {
        fprintf(stderr, "config file could not be found: %s\n", file_name);
        return -1;
    }

    if (!config_read_file(&cfg, file_name)) {
        fprintf(stderr, "%s:%d - %s\n", config_error_file(&cfg),
                config_error_line(&cfg), config_error_text(&cfg));
        config_destroy(&cfg);
        return -1;
    }

    if (!config_lookup_string(&cfg, RSS_URL, &str)) {
        fprintf(stderr, "No '%s' setting in configuration file.\n", RSS_URL);
        return -1;
    }
    rss_config->rss = strdup(str);

    if (!config_lookup_int(&cfg, RSS_TIMEOUT, &(rss_config->timeout))) {
        fprintf(stderr, "No '%s' setting in configuration file.\n", RSS_TIMEOUT);
        return -1;
    }

    if(!config_lookup_string(&cfg, OUTPUT, &str)) {
        fprintf(stderr, "No '%s' setting in configuration file.\n", OUTPUT);
        return -1;
    }
    rss_config->output = strdup(str);

    if(!config_lookup_string(&cfg, FETCH_STORE_DIR, &str)) {
        fprintf(stderr, "No '%s' setting in configuration file.\n", FETCH_STORE_DIR);
        return -1;
    }
    rss_config->fetch_store_dir = strdup(str);

    setting = config_lookup(&cfg, RECORD_ENTRIES);
    if (setting == NULL) {
        fprintf(stderr, "No '%s' setting in configuration file.\n", RECORD_ENTRIES);
        return -1;
    }

    int count = config_setting_length(setting);
    int i;

    rss_config->entry_config_array.items = calloc(count, sizeof(struct entry_config));

    int correct_count = 0;

    for (i = 0; i < count; ++i) {
        const char *filter;

        config_setting_t *entry = config_setting_get_elem(setting, i);
        if (!config_setting_lookup_string(entry, "name", &str)) {
            fprintf(stderr, "Entry does not contains required field '%s'.\n", "name");
            continue;
        }
        rss_config->entry_config_array.items[correct_count].name = strdup(str);

        if (!config_setting_lookup_string(entry, "query", &str)) {
            fprintf(stderr, "Entry does not contains required field '%s'.\n", "query");
            continue;
        }
        rss_config->entry_config_array.items[correct_count].query = strdup(str);

        rss_config->entry_config_array.items[correct_count].filter.type = query;

        if (config_setting_lookup_string(entry, "filter", &filter)) {
            enum filter_type filter_type = str2enum(filter);
            switch (filter_type) {
                case replace:
                case regex:
                    if (!config_setting_lookup_string(entry, "options", &str)) {
                        //todo:error
                    }
                    rss_config->entry_config_array.items[correct_count].filter.options = strdup(str);
                    rss_config->entry_config_array.items[correct_count].filter.type = filter_type;
                    correct_count++;
                    break;
                case trim:
                    if (config_setting_lookup_string(entry, "options", &str)) {
                        //todo:error
                    }
                    rss_config->entry_config_array.items[correct_count].filter.options = strdup(str);
                    rss_config->entry_config_array.items[correct_count].filter.type = filter_type;
                    correct_count++;
                    break;
                default:
                    //todo:error
                    break;

            }

        } else {
            correct_count++;
        }

    }
    if (count != correct_count) {
        rss_config->entry_config_array.items = realloc(rss_config->entry_config_array.items, sizeof(struct entry_config) * correct_count);
    }

    rss_config->entry_config_array.size = correct_count;


    config_destroy(&cfg);

    return 0;
}

void clean_config(struct fetch_config * rss_config){
    for (int i = 0; i < rss_config->entry_config_array.size; i++) {
        struct entry_config *config = &rss_config->entry_config_array.items[i];

        if (config->filter.options != NULL) {
            free((void *) config->filter.options);
        }
        free(config->name);
        free(config->query);
    }

    rss_config->entry_config_array.size = 0;
    free(rss_config->rss);
    free(rss_config->entry_config_array.items);
    free(rss_config->output);
    free(rss_config->fetch_store_dir);


}

