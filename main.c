#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>

#include "http_fetch.h"
#include "cbazos.h"
#include "args.h"
#include "rss_fetch.h"
#include "config.h"
#include "json_generator.h"

void clean_rss_items(struct rss_item_array *rss_item_array) {
    if (rss_item_array->size == 0) {
        return;
    }
    for (int i = 0; i < rss_item_array->size; i++) {
        struct rss_item *item = &rss_item_array->items[i];
        if (item->title != NULL) {
            free(item->title);
        }
        if (item->desc != NULL) {
            free(item->desc);
        }
        if (item->guid != NULL) {
            free(item->guid);
        }
        if (item->pub_date != NULL) {
            free(item->pub_date);
        }
        if (item->link != NULL) {
            free(item->link);
        }
    }
    free(rss_item_array->items);
}


int check_item_for_errors(struct collected_data_item *item) {
    int entries_with_error = 0;
    for (int i = 0; i < item->entry_array.size; i++) {
        if (item->entry_array.items[i].error != NULL && item->entry_array.items[i].error[0] != '\0') {
            entries_with_error++;
        }
    }
    return entries_with_error;
}

void print_item(struct collected_data_item *item) {
    int entries_with_error = check_item_for_errors(item);


    printf("\tfetch time: %ld.%06ld process time: %ld.%06ld failed: %d\n", (long int) item->fetch_time.tv_sec, (long int) item->fetch_time.tv_usec,
           (long int) item->process_time.tv_sec, (long int) item->process_time.tv_usec, entries_with_error);
    for (int j = 0; j < item->entry_array.size; j++) {
        struct collected_data_entry *entry = &item->entry_array.items[j];

        if (entry->error != NULL && entry->error[0] != '\0') {
            printf("\t\t%s => %.*s (%s)\n", entry->name, 100, entry->value, entry->error);
        } else {
            printf("\t\t%s => %.*s\n", entry->name, 100, entry->filtered_value == NULL ? entry->value : entry->filtered_value);
        }
    }
}

void update_errors(struct fetch_data *fetch_data, struct collected_data_item *item) {
    int found_error = 0;
    for (int i = 0; i < item->entry_array.size; i++) {
        if (item->entry_array.items[i].error != NULL) {
            found_error++;
        }
    }
    if (fetch_data->errors.size < 1) {
        fetch_data->errors.items = calloc(found_error, sizeof(struct error_item));
    } else {
        fetch_data->errors.items = realloc(fetch_data->errors.items, fetch_data->errors.size + found_error);
    }
    for (int i = 0; i < item->entry_array.size; i++) {
        if (item->entry_array.items[i].error != NULL) {
            fetch_data->errors.items[fetch_data->errors.size + i].name = strdup(item->entry_array.items[i].name);
            fetch_data->errors.items[fetch_data->errors.size + i].error_msg = strdup(item->entry_array.items[i].error);
        }
    }

    fetch_data->errors.size = fetch_data->errors.size + found_error;
}


int main(int argc, char **argv) {
    struct arguments arguments;
    struct fetch_config fetch_config;
    struct fetch_data fetch_data = {.fetched_item_array.size=0, .fetched_items_total=0, .fetched_items_new=0, .errors.size=0};
    struct collected_data_item fetch_item = {.entry_array.size = 0, .process_time = {.tv_sec = 0, .tv_usec=0}, .fetch_time = {.tv_usec = 0, .tv_sec=0}};
    struct rss_item_array rss_items = {.size = 0};

    struct timeval start, end, total_start, total_end;

    int parsed = parse_args(argc, argv, &arguments);
    if (parsed < 0) {
        exit(EXIT_FAILURE);
    }

    gettimeofday(&total_start, NULL);

    if (arguments.test_query == NULL) {
        if (parse_config(&fetch_config, arguments.config_file) < 0) {
            exit(EXIT_FAILURE);
        }
        //check fetch output directory
        struct stat stat_dir;

        if (strcmp(arguments.output, "json") == 0
            && (stat(fetch_config.fetch_store_dir, &stat_dir) != 0
                || !S_ISDIR(stat_dir.st_mode))) {
            fprintf(stderr, "fetch store directory does not exists.\n");
            exit(EXIT_FAILURE);
        }

    } else {
        fetch_config.entry_config_array.items = calloc(sizeof(struct entry_config), 1);
        fetch_config.entry_config_array.items[0].name = "test";
        fetch_config.entry_config_array.items[0].query = arguments.test_query;
        fetch_config.entry_config_array.size = 1;
    }

    if (arguments.test_file != NULL) {
        printf("running config agains test file: %s\n", arguments.test_file);

        FILE *f = fopen(arguments.test_file, "rb");
        if (f == NULL) {
            perror("Error: ");
            exit(EXIT_FAILURE);
        }
        fseek(f, 0, SEEK_END);
        size_t file_length = ftell(f);
        fseek(f, 0, SEEK_SET);
        char *buff = (char *) malloc(file_length + 1);
        fread(buff, 1, file_length, f);
        fclose(f);

        fetch_item.xml_buffer.memory = buff;
        fetch_item.xml_buffer.size = file_length;

        fetch_data.fetched_items_total = 1;
        fetch_data.fetched_item_array.size = 1;
        fetch_data.source_url = "TEST";
    } else {
        gettimeofday(&start, NULL);
        fetch_data.fetched_items_total = fetch_rss(&fetch_config, &rss_items);

        gettimeofday(&end, NULL);
        timersub(&end, &start, &fetch_data.rss_fetch_exec_time);

        fetch_data.fetched_items_new = fetch_data.fetched_items_total;
        fetch_data.source_url = fetch_config.rss;

        fetch_data.fetched_item_array.size = fetch_data.fetched_items_new;
        time(&fetch_data.fetch_timestamp);
    }

    initialize_url_fetcher();

    for (int i = 0; i < fetch_data.fetched_items_total; i++) {


        gettimeofday(&start, NULL);
        if (arguments.test_file == NULL) {
            if (fetch_url(rss_items.items[i].link, &(fetch_item.xml_buffer)) < 0) {
                continue;
            }
        }

        gettimeofday(&end, NULL);
        timersub(&end, &start, &fetch_item.fetch_time);

        fetch_item.entry_array.items = (struct collected_data_entry *) calloc(fetch_config.entry_config_array.size, sizeof(struct collected_data_entry));
        fetch_item.entry_array.size = fetch_config.entry_config_array.size;

        gettimeofday(&start, NULL);
        if (process_xml_memory_buffer(&fetch_item, fetch_config.entry_config_array.items, fetch_config.entry_config_array.size) < 0) {
            fprintf(stderr, "failed to parse xml");
        }
        gettimeofday(&end, NULL);
        timersub(&end, &start, &fetch_item.process_time);

        clean_fetcher_buffer(&(fetch_item.xml_buffer));

        update_errors(&fetch_data, &fetch_item);

        if (arguments.test_file == NULL && strcmp(arguments.output, "json")) {
            int name_length = snprintf(NULL, 0, "%s/%d.json", fetch_config.fetch_store_dir, rss_items.items[i].item_id);

            char *filepath = malloc(name_length + 1);
            sprintf(filepath, "%s/%d.json", fetch_config.fetch_store_dir, rss_items.items[i].item_id);

            FILE *fp = fopen(filepath, "w");
            const char *json = json_generate_fetch_data(&fetch_item, &rss_items.items[i]);
            if (fp != NULL && json != NULL) {
                fputs(json, fp);
                fclose(fp);
            }

            free(filepath);
            free((void *)json);
        } else {
            print_item(&fetch_item);
        }
    }

    destroy_url_fetcher();

    gettimeofday(&total_end, NULL);
    timersub(&total_end, &total_start, &fetch_data.total_process_time);

    printf("rss: %s fetch time: %ld.%06ld\n", fetch_data.source_url, (long int) fetch_data.rss_fetch_exec_time.tv_sec, (long int) fetch_data.rss_fetch_exec_time.tv_usec);
    printf("total time: %ld.%06ld with total errors: %d\n", (long int) fetch_data.total_process_time.tv_sec, (long int) fetch_data.total_process_time.tv_usec,
           fetch_data.errors.size);

    /*clean up*/
    clean_data_collection_item(&fetch_item);
    clean_rss_items(&rss_items);
    clean_config(&fetch_config);

    printf("done\n");
    return (EXIT_SUCCESS);
}

