//
// Created by martin on 2/3/2021.
//
#include <stdio.h>
#include <time.h>
#include <string.h>

#include <json-c/json.h>

#include "json_generator.h"
#include "types.h"

char *nullable_string(char *str) {
    if (str == NULL) {
        return "(NULL)";
    }
    return str;
}

const char *json_generate_fetch_data(struct collected_data_item *item, struct rss_item *rss_item) {
    struct json_object *json = json_object_new_object();
    char time_str[100];

    int time_str_length = sprintf(time_str, "%ld.%06ld", (long int) item->fetch_time.tv_sec, (long int) item->fetch_time.tv_usec);


    if (time_str_length < 0 || time_str_length > 100) {
        fprintf(stderr, "could not sprintf fetch time or string is too long.\n");
    }

    char buff[27];
    time_t now = time(NULL);

    struct tm * tm = localtime(&now);
    strftime(buff, 27, "%Y-%m-%d %H:%M:%S %z", tm);

    json_object_object_add(json, RSS_TITLE, json_object_new_string(nullable_string(rss_item->title)));
    json_object_object_add(json, RSS_LINK, json_object_new_string(nullable_string(rss_item->link)));
    json_object_object_add(json, RSS_PUB_DATE, json_object_new_string(nullable_string(rss_item->pub_date)));
    json_object_object_add(json, RSS_ITEM_ID, json_object_new_int(rss_item->item_id));
    json_object_object_add(json, RSS_FETCH_TIME, json_object_new_string(time_str));
    json_object_object_add(json, FETCH_TIMESTAMP, json_object_new_string(buff));

    struct json_object *entries = json_object_new_array();
    for (int i = 0; i < item->entry_array.size; i++) {
        struct json_object *entry = json_object_new_object();

        json_object_object_add(entry, NAME, json_object_new_string(nullable_string(item->entry_array.items[i].name)));
        if (item->entry_array.items[i].value != NULL) { json_object_object_add(entry, VALUE, json_object_new_string(item->entry_array.items[i].value)); }
        if (item->entry_array.items[i].filtered_value) { json_object_object_add(entry, FILTERED_VALUE, json_object_new_string(item->entry_array.items[i].filtered_value)); }
        if (item->entry_array.items[i].error) { json_object_object_add(entry, ERROR, json_object_new_string(item->entry_array.items[i].error)); }

        json_object_array_add(entries, entry);
    }

    json_object_object_add(json, ENTRIES, entries);

    const char * json_str = strdup(json_object_to_json_string_ext(json, JSON_C_TO_STRING_PRETTY));

    json_object_put(json);

    return json_str;
}

