#include <unistd.h>
#include <stdio.h>
#include <ctype.h>

#include "args.h"

int parse_args(int argc, char **argv, struct arguments *arguments) {
    int c;

    arguments->list_output = 0;
    arguments->config_file = DEFAULT_CONFIG;
    arguments->output = DEFAULT_OUTPUT;
    arguments->test_file = NULL;
    arguments->test_query = NULL;

    int changed = 0;

    while ((c = getopt(argc, argv, "c:o:t:q:l")) != -1) {
        switch (c) {
            case 'c':
                arguments->config_file = optarg;
                changed++;
                break;
            case 'o':
                arguments->output = optarg;
                changed++;
                break;
            case 't':
                arguments->test_file = optarg;
                changed++;
                break;
            case 'q':
                arguments->test_query = optarg;
                changed++;
                break;
            case 'l':
                arguments->list_output = 1;
                break;
            case '?':
                if (optopt == 'c' || optopt == 'o')
                    fprintf(stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint (optopt))
                    fprintf(stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf(stderr,
                            "Unknown option character `\\x%x'.\n",
                            optopt);
                return -1;
            default:
                return -1;
        }
    }

    return changed;
}

