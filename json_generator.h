//
// Created by martin on 2/3/2021.
//

#ifndef CBAZOS_JSON_GENERATOR_H
#define CBAZOS_JSON_GENERATOR_H

#include "types.h"

#define RSS_TITLE "title"
#define RSS_LINK "link"
#define RSS_GUID "guid"
#define RSS_PUB_DATE "pub_date"
#define RSS_ITEM_ID "item_id"
#define RSS_FETCH_TIME "fetch_time"

#define FETCH_TIMESTAMP "timestamp"

#define ENTRIES "entries"

#define NAME "name"
#define VALUE "value"
#define FILTERED_VALUE "filtered_value"
#define ERROR "error"

const char *json_generate_fetch_data(struct collected_data_item *item, struct rss_item *rss_item);

#endif //CBAZOS_JSON_GENERATOR_H
