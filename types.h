//
// Created by martin on 2/3/2021.
//

#ifndef CBAZOS_TYPES_H
#define CBAZOS_TYPES_H

#include <sys/time.h>

struct memory_struct {
    char *memory;
    size_t size;
};

struct collected_data_entry {
    char *name;
    char *error;
    char *value;
    char *filtered_value;
};

struct collected_data_entry_array {
    int size;
    struct collected_data_entry *items;
};



struct collected_data_item {
    struct memory_struct xml_buffer;

    struct timeval fetch_time;
    struct timeval process_time;

    struct collected_data_entry_array entry_array;
};

struct collected_data_item_array {
    int size;
    struct collected_data_item *items;
};

struct error_item {
    char *name;
    char *error_msg;
};

struct error_item_array {
    struct error_item *items;
    int size;
};

struct fetch_data {
    const char *source_url;
    time_t fetch_timestamp;
    int fetched_items_total;
    int fetched_items_new;

    struct timeval rss_fetch_exec_time;
    struct timeval total_process_time;

    struct error_item_array errors;

    struct collected_data_item_array fetched_item_array;
};

enum filter_type {
    regex, replace, trim, query
};

typedef struct filter {
    enum filter_type type;
    const char *options;
} filter_t;

typedef struct entry_config {
    char *name;
    char *query;
    filter_t filter;
} entry_config;

struct entry_config_array{
    int size;
    entry_config *items;
};

struct fetch_config {
    char *rss;
    int timeout;
    char *output;
    char *fetch_store_dir;
    struct entry_config_array entry_config_array;
};

struct rss_item_array {
    struct rss_item *items;
    int size;
};

struct rss_item {
    char *title;
    char *link;
    char *desc;
    char *pub_date;
    char *guid;
    unsigned int item_id;
};

struct arguments {
    char *config_file;
    char *output;
    short list_output;
    char *test_file;
    char *test_query;
};

#endif //CBAZOS_TYPES_H
