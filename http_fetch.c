//
// Created by martin on 1/23/2021.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <curl/curl.h>

#include "cbazos.h"
#include "http_fetch.h"


static size_t
write_memory_callback(void *contents, size_t size, size_t nmemb, void *userp) {
    size_t realsize = size * nmemb;
    struct memory_struct *mem = (struct memory_struct *) userp;

    char *ptr = realloc(mem->memory, mem->size + realsize + 1);
    if (ptr == NULL) {
        /* out of memory! */
        printf("not enough memory (realloc returned NULL)\n");
        return 0;
    }

    mem->memory = ptr;
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

void initialize_url_fetcher(){
    curl_global_init(CURL_GLOBAL_ALL );
}

int fetch_url(const char *url, struct memory_struct *chunk) {
    CURL *curl_handle;
    CURLcode res;

    chunk->memory = malloc(1);  /* will be grown as needed by the realloc above */
    chunk->size = 0;    /* no data at this point */


    /* init the curl session */
    curl_handle = curl_easy_init();

    /* specify URL to get */
    curl_easy_setopt(curl_handle, CURLOPT_URL, url);

    curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYPEER, 0);

    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_memory_callback);

    /* we pass our 'chunk' struct to the callback function */
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *) chunk);

    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, USER_AGENT);

    /* get it! */
    res = curl_easy_perform(curl_handle);

    /* check for errors */
    if (res != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));

        return -1;
    }

    curl_easy_cleanup(curl_handle);

    return 0;
}

void clean_fetcher_buffer(struct memory_struct * chunk){
    free(chunk->memory);
    chunk->size = 0;
}

void destroy_url_fetcher(){
    curl_global_cleanup();
}

