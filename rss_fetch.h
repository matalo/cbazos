//
// Created by martin on 1/17/2021.
//

#ifndef CBAZOS_RSS_FETCH_H
#define CBAZOS_RSS_FETCH_H

int fetch_rss(struct fetch_config *config, struct rss_item_array *rss_items);

#endif //CBAZOS_RSS_FETCH_H
