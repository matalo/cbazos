//
// Created by martin on 1/15/2021.
//

#ifndef CBAZOS_ARGS_H
#define CBAZOS_ARGS_H

#define DEFAULT_CONFIG "config.ini"
#define DEFAULT_OUTPUT "json-store"

#include "types.h"

int parse_args(int argc, char **argv, struct arguments *arguments);

#endif //CBAZOS_ARGS_H
