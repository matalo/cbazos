//
// Created by martin on 1/23/2021.
//

#ifndef CBAZOS_HTTP_FETCH_H
#define CBAZOS_HTTP_FETCH_H

#include "types.h"

#define USER_AGENT "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.2 Safari/605.1.15"

int fetch_url(const char *, struct memory_struct *);

void initialize_url_fetcher();

void clean_fetcher_buffer(struct memory_struct *chunk);

void destroy_url_fetcher();

#endif //CBAZOS_HTTP_FETCH_H
